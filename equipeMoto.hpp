//
//  equipeMoto.hpp
//  exam-rabatel
//
//  Created by RABATEL MARTY on 04/07/2024.
//

#ifndef equipeMoto_hpp
#define equipeMoto_hpp

#include <stdio.h>
#include "personne.hpp"

namespace Personne {
    class EquipeMotoClass : public PersonneClass {
        private:
            //std::string nom;
        public:
            int maxPilote = 3;
    };
}

#endif /* equipeMoto_hpp */
