//
//  personne.hpp
//  exam-rabatel
//
//  Created by RABATEL MARTY on 04/07/2024.
//

#ifndef personne_hpp
#define personne_hpp

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;
namespace Personne {
    class PersonneClass {
        private:
            std::string nom;
        public:
            PersonneClass(std::string nom);
            std::string GetNom() {
                return this->nom;
            }
            // Fonction pour créer une personne
            PersonneClass* CreerPersonne() {
                std::string nom;
                std::cout << "Entrez le nom de la personne: ";
                std::getline(std::cin, nom);

                // Créer dynamiquement une nouvelle Personne
                PersonneClass* nouvellePersonne = new PersonneClass(nom);

                return nouvellePersonne;
            }
    };
}

#endif /* personne_hpp */
