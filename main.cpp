//
//  main.cpp
//  exam-rabatel
//
//  Created by RABATEL MARTY on 04/07/2024.
//
#include <iostream>
#include "personne.hpp"
#include "personne.cpp"

using namespace std;
using namespace Personne;

int main(int argc, const char * argv[]) {
    PersonneClass pilote1(PersonneClass("Fabio"));
    //cout << pilote1.GetNom() << endl;
    std::cout << "Adresse de pilote_1: " << pilote1.GetNom() << " : " << static_cast<const void*>(&pilote1) << std::endl;
    
    // Dynamique pilote
    PersonneClass* pilote2;
    pilote2 = new PersonneClass("Louis");
    std::cout << "Adresse de pilote_2: " << pilote2->GetNom() << " : " << static_cast<const void*>(&pilote2) << std::endl;
    
    // Créer une personne dynamiquement
    PersonneClass* pilote = CreerPersonne();

}
